package org.numisoft.vocabuilder.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.numisoft.vocabuilder.Const
import retrofit2.http.GET
import retrofit2.http.Query

interface YandexApi {

    @GET("lookup")
    suspend fun getTranslation(
        @Query("text") text: String,
        @Query("key") key: String = Const.YandexApi.KEY,
        @Query("lang") lang: String = Const.YandexApi.LANG_EN_RU
    ): YandexWord
}

@JsonClass(generateAdapter = true)
data class YandexWord(
    @field:Json(name = "def") val def: List<Def>
)

@JsonClass(generateAdapter = true)
data class Def(
    @field:Json(name = "text") val text: String,
    @field:Json(name = "pos") val pos: String,
    @field:Json(name = "ts") val ts: String?,
    @field:Json(name = "tr") val tr: List<Tr>
)

@JsonClass(generateAdapter = true)
data class Tr(
    @field:Json(name = "text") val text: String,
    @field:Json(name = "mean") val mean: List<Mean>?,
    @field:Json(name = "ex") val ex: List<Ex>?
)

@JsonClass(generateAdapter = true)
data class Mean(
    @field:Json(name = "text") val text: String
)

@JsonClass(generateAdapter = true)
data class Ex(
    @field:Json(name = "text") val text: String,
    @field:Json(name = "tr") val tr: List<Tr>
)