package org.numisoft.vocabuilder.repository

data class Word(
    val text: String,
    val definitions: List<Definition>
)

data class Definition(
    val pos: String,
    val transcription: String?,
    val translation: String,
    val synonym: String?,
    val example: String?
)