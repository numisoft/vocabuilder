package org.numisoft.vocabuilder.repository

import org.numisoft.vocabuilder.database.LocalDefinition
import org.numisoft.vocabuilder.database.LocalWord
import org.numisoft.vocabuilder.database.LocalWordWithDefinitions
import org.numisoft.vocabuilder.listToString
import org.numisoft.vocabuilder.network.Def
import org.numisoft.vocabuilder.network.Ex
import org.numisoft.vocabuilder.network.Mean
import org.numisoft.vocabuilder.network.YandexWord

fun YandexWord.toWord(text: String) = Word(
    text = text,
    definitions = def.map { it.toDefinition() }
)

fun Def.toDefinition() = Definition(
    pos = pos,
    transcription = "[ $ts ]",
    translation = tr.map { it.text }.listToString(),
    synonym = getSynonym(this),
    example = getExample(this)
)

fun getSynonym(def: Def): String? {

    val synList = mutableListOf<Mean>()

    def.tr.forEach {
        it.mean?.forEach {
            synList.add(it)
        }
    }

    return if (synList.isNotEmpty()) { synList.map { it.text }.listToString() } else null
}

fun getExample(def: Def): String? {

    val exList = mutableListOf<Ex>()

    def.tr.forEach {
        it.ex?.forEach {
            exList.add(it)
        }
    }

    return if (exList.isNotEmpty()) {

        var exString = ""

        exList.forEachIndexed { index, ex ->
            exString = exString.plus("${ex.text} - ${ex.tr.first().text}")
            if (index != exList.lastIndex) exString = exString.plus("\n")
        }

        exString

    } else null

}

fun Word.toLocalWordWithDefinitions() = LocalWordWithDefinitions(
    word = LocalWord(text),
    definitions = definitions.map { it.toLocalDefinition(text) }
)

fun Word.toLocalWord() = LocalWord(text)

fun Definition.toLocalDefinition(text: String) = LocalDefinition(
    word = text,
    pos = pos,
    transcription = transcription,
    translation = translation,
    synonym = synonym,
    example = example
)

fun LocalWordWithDefinitions.toWord() = Word(
    text = word.text,
    definitions = definitions.map { it.toDefinition() }
)

fun LocalDefinition.toDefinition() = Definition(
    pos = pos,
    transcription = transcription,
    translation = translation,
    synonym = synonym,
    example = example
)