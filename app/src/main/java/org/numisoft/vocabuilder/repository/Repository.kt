package org.numisoft.vocabuilder.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.numisoft.vocabuilder.Resource
import org.numisoft.vocabuilder.database.WordDao
import org.numisoft.vocabuilder.network.YandexApi

class Repository(
    private val yandexApi: YandexApi,
    private val wordDao: WordDao
) {

    fun getWord(text: String): Flow<Resource<Word>> = flow {
        emit(
            try {
                val word = yandexApi.getTranslation(text).toWord(text)
                if (word.definitions.isNotEmpty()) {
                    Resource.Success(word)
                } else {
                    Resource.EmptyState<Word>(word.text)
                }
            } catch (exception: Throwable) {
                exception.printStackTrace()
                Resource.Error<Word>(exception)
            }
        )
    }

    fun getLocalWords(): Flow<List<Word>> =
        wordDao.getWords()
            .map { list ->
                list.map { it.toWord() }
            }

    suspend fun saveLocalWord(word: Word) =
        wordDao.insertWordWithDefinitions(word.toLocalWordWithDefinitions())

    suspend fun deleteLocalWord(word: Word) =
        wordDao.deleteLocalWord(word.toLocalWordWithDefinitions())
}