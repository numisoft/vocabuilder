package org.numisoft.vocabuilder.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

const val DB_TABLE_WORD = "word"
const val DB_TABLE_DEFINITION = "definition"

@Database(entities = [LocalWord::class, LocalDefinition::class], version = 1)
abstract class VbDatabase() : RoomDatabase() {

    abstract fun wordDao(): WordDao
}

data class LocalWordWithDefinitions(

    @Embedded
    val word: LocalWord,

    @Relation(parentColumn = "text", entityColumn = "word")
    val definitions: List<LocalDefinition>

)

@Entity(tableName = DB_TABLE_WORD)
data class LocalWord(
    @PrimaryKey
    val text: String
)

@Entity(tableName = DB_TABLE_DEFINITION, primaryKeys = ["pos", "translation"])
data class LocalDefinition(
    val word: String,
    val pos: String,
    val transcription: String?,
    val translation: String,
    val synonym: String?,
    val example: String?
)

@Dao
interface WordDao {

    @Query("SELECT * FROM $DB_TABLE_WORD")
    fun getWords(): Flow<List<LocalWordWithDefinitions>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWord(word: LocalWord)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDefinition(definition: LocalDefinition)

    @Transaction
    suspend fun insertWordWithDefinitions(wordWithDefinitions: LocalWordWithDefinitions) {
        insertWord(wordWithDefinitions.word)
        wordWithDefinitions.definitions.forEach {
            insertDefinition(it)
        }
    }

    @Transaction
    suspend fun deleteLocalWord(word: LocalWordWithDefinitions) {
        deleteWord(word.word)
        word.definitions.forEach { deleteDefinition(it) }
    }

    @Delete
    suspend fun deleteWord(word: LocalWord)

    @Delete
    suspend fun deleteDefinition(definition: LocalDefinition)
}