package org.numisoft.vocabuilder

sealed class Resource<T>(
    val data: T? = null,
    val notFound: String? = null,
    val exception: Throwable? = null
) {
    class Success<T>(data: T) : Resource<T>(data = data)
    class EmptyState<T>(notFound: String) : Resource<T>(notFound = notFound)
    class Error<T>(exception: Throwable) : Resource<T>(exception =  exception)
}