package org.numisoft.vocabuilder

object Const {

    object YandexApi {
        const val BASE_URL = "https://dictionary.yandex.net/api/v1/dicservice.json/"
        const val KEY = "dict.1.1.20200416T221803Z.1070f517a887f5fb.d686adbeae7ef88e8c06ffc862f81b7da0fc5c23"
        const val LANG_EN_RU = "en-ru"
    }
}