package org.numisoft.vocabuilder

import android.content.Context
import android.widget.Toast
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import org.numisoft.vocabuilder.repository.Word

typealias WordCallback = (Word) -> Unit

fun ObservableBoolean.toggle() = set(!get())

fun Observable.onChanged(action: () -> Unit) {
    addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            action.invoke()
        }
    })
}

fun List<Any>.listToString() = toString().removeSurrounding("[", "]")

fun Context.showToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()