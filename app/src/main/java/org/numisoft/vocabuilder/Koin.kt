package org.numisoft.vocabuilder

import androidx.room.Room
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.numisoft.vocabuilder.database.VbDatabase
import org.numisoft.vocabuilder.repository.Repository
import org.numisoft.vocabuilder.network.YandexApi
import org.numisoft.vocabuilder.ui.MainViewModel
import org.numisoft.vocabuilder.ui.list.ListViewModel
import org.numisoft.vocabuilder.ui.search.SearchViewModel
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = module {

    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl(Const.YandexApi.BASE_URL)
            .client(get())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    single { get<Retrofit>().create(YandexApi::class.java) }
}

val databaseModule = module {
    single { get<VbDatabase>().wordDao() }
    single { Room.databaseBuilder(get(), VbDatabase::class.java, "vb-database").build() }
}

val uiModule = module {

    single {
        Repository(
            yandexApi = get(),
            wordDao = get()
        )
    }
    viewModel { MainViewModel() }
    viewModel { SearchViewModel(repository = get()) }
    viewModel { ListViewModel(repository = get()) }
}