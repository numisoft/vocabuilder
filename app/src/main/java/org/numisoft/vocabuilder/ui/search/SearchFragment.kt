package org.numisoft.vocabuilder.ui.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.numisoft.vocabuilder.R
import org.numisoft.vocabuilder.databinding.FragmentSearchBinding
import org.numisoft.vocabuilder.onChanged
import org.numisoft.vocabuilder.showToast

class SearchFragment : Fragment() {

    private val viewModel by viewModel<SearchViewModel>()

    private lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate<FragmentSearchBinding>(
            inflater,
            R.layout.fragment_search,
            container,
            false
        )

        viewModel.run {
            binding.viewModel = this
            hideKeyboard.onChanged { hideKeyboard() }
            showEmptyState.onChanged { showEmptyState.get()?.let { showNotFoundDialog(it) } }
            showError.onChanged { showError.get()?.let { showError(it) } }
        }

        ViewCompat.setOnApplyWindowInsetsListener(binding.constraint) { view, insets ->
            view.updatePadding(top = insets.systemWindowInsetTop)
            Log.wtf(javaClass.simpleName, insets.systemWindowInsetTop.toString())
            insets
        }

        return binding.root
    }

    private fun showNotFoundDialog(word: String) = context?.let {
        AlertDialog.Builder(it).apply {
            setMessage("Translation for [ $word ] not detected!")
            setPositiveButton("Nevermind") { _, _ -> viewModel.showEmptyState.set(null) }
            setOnDismissListener { viewModel.showEmptyState.set(null) }
            show()
        }
    }

    private fun showError(message: String) = context?.showToast("ERROR: $message")

    private fun hideKeyboard() = binding.inputEditText.onEditorAction(EditorInfo.IME_ACTION_DONE)
}