package org.numisoft.vocabuilder.ui.list

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nitrico.lastadapter.LastAdapter
import org.numisoft.vocabuilder.R
import org.numisoft.vocabuilder.databinding.FragmentListBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.numisoft.vocabuilder.BR
import org.numisoft.vocabuilder.databinding.ViewWordCardBinding
import org.numisoft.vocabuilder.onChanged
import org.numisoft.vocabuilder.repository.Word
import org.numisoft.vocabuilder.ui.view.WordCard
import org.numisoft.vocabuilder.ui.view.WordCardViewModel

class ListFragment : Fragment(R.layout.fragment_list) {

    val viewModel by viewModel<ListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentListBinding>(
            inflater, R.layout.fragment_list, container, false
        )
        binding.viewModel = viewModel

        binding.recycler.run {
            adapter = LastAdapter(viewModel.items, BR.viewModel, stableIds = true)
                .map<WordItemViewModel>(R.layout.item_word)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        viewModel.showDeleteDialog.run {
            set(null)
            onChanged {
                viewModel.showDeleteDialog.get()?.let {
                    showDeleteDialog(it)
                }
            }
        }

        viewModel.showDetailDialog.run {
            set(null)
            onChanged {
                viewModel.showDetailDialog.get()?.let {
                    showDetailDialog(it)
                }
            }
        }

        ViewCompat
            .setOnApplyWindowInsetsListener(binding.recycler) { view, insets ->
                view.updatePadding(top = insets.systemWindowInsetTop)
                Log.wtf(javaClass.simpleName, insets.systemWindowInsetTop.toString())
                insets
            }

        return binding.root
    }

    private fun showDeleteDialog(word: Word) = context?.let {
        AlertDialog.Builder(it).apply {
            setMessage("Are you sure to detele [ ${word.text} ] ?")
            setPositiveButton("Yes") { _, _ -> viewModel.deleteWord(word) }
            setNegativeButton("No") { _, _ -> viewModel.showDeleteDialog.set(null) }
            setOnDismissListener { viewModel.showDeleteDialog.set(null) }
            show()
        }
    }

    private fun showDetailDialog(word: Word) = context?.let {
        val card = WordCard(it)
        card.setViewModel(WordCardViewModel(ObservableField(word)))
        val dialog = Dialog(it, R.style.Theme_AppCompat_Light_Dialog_Alert).apply {
            setContentView(card)
            window?.setBackgroundDrawable(null)
            setOnDismissListener { viewModel.showDetailDialog.set(null) }
        }
        dialog.show()
    }
}