package org.numisoft.vocabuilder.ui.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.StableId
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.numisoft.vocabuilder.BR
import org.numisoft.vocabuilder.R
import org.numisoft.vocabuilder.databinding.ViewWordCardBinding
import org.numisoft.vocabuilder.onChanged
import org.numisoft.vocabuilder.repository.Definition
import org.numisoft.vocabuilder.repository.Repository
import org.numisoft.vocabuilder.repository.Word

@BindingAdapter("viewModel")
fun WordCard.setVM(viewModel: WordCardViewModel) = setViewModel(viewModel)

class WordCard @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding: ViewWordCardBinding =
        ViewWordCardBinding.inflate(LayoutInflater.from(context), this, true)

    fun setViewModel(viewModel: WordCardViewModel) {
        binding.viewModel = viewModel

        binding.recycler.run {
            adapter = LastAdapter(viewModel.definitions, BR.viewModel, stableIds = true)
                .map<DefinitionItemViewModel>(R.layout.item_definition)
            layoutManager = LinearLayoutManager(context)
        }
    }
}

class WordCardViewModel(
    val word: ObservableField<Word>,
    val coroutineScope: CoroutineScope? = null
) : KoinComponent {

    val repository by inject<Repository>()
    val definitions = ObservableArrayList<DefinitionItemViewModel>()

    init {
        updateDefinitions()
        word.onChanged {
            updateDefinitions()
            Log.wtf(javaClass.simpleName, "onChanged")
        }
    }

    private fun updateDefinitions() {
        Log.wtf(javaClass.simpleName, "updateDefinitions")
        word.get()?.let {
            definitions.run {
                clear()
                addAll(it.definitions.map { def ->
                    DefinitionItemViewModel(def)
                })
            }
        }
    }

    fun onAddClick() = word.get()?.let {
        coroutineScope?.launch {
            repository.saveLocalWord(it)
        }
    }
}

class DefinitionItemViewModel(val definition: Definition) : StableId {

    override val stableId: Long = definition.hashCode().toLong()
}