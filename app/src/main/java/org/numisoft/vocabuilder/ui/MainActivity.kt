package org.numisoft.vocabuilder.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commitNow
import androidx.lifecycle.ViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.numisoft.vocabuilder.R
import org.numisoft.vocabuilder.ui.list.ListFragment
import org.numisoft.vocabuilder.ui.search.SearchFragment

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpFullScreenMode()
        setupBottomNavigation()
        if (savedInstanceState == null) addFragments()
    }

    private fun setupBottomNavigation() = findViewById<BottomNavigationView>(R.id.navigation).run {
        setOnNavigationItemSelectedListener {
            switchFragment(it.itemId.toString())
        }
    }

    private fun addFragments() = supportFragmentManager.commitNow {
        ListFragment().run {
            add(R.id.container, this, R.id.menu_list.toString())
            hide(this)
        }
        add(R.id.container, SearchFragment(), R.id.menu_search.toString())
    }

    private fun switchFragment(newActiveTag: String): Boolean {
        if (newActiveTag != viewModel.activeTag) {
            supportFragmentManager.commitNow {
                supportFragmentManager.findFragmentByTag(viewModel.activeTag)?.let {
                    hide(it)
                }
                supportFragmentManager.findFragmentByTag(newActiveTag)?.let {
                    show(it)
                }
                viewModel.activeTag = newActiveTag
            }
        }
        return true
    }

    private fun setUpFullScreenMode() = window.apply {
        clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        statusBarColor = Color.TRANSPARENT
    }
}

class MainViewModel(
    var activeTag: String = R.id.menu_search.toString()
) : ViewModel()