package org.numisoft.vocabuilder.ui.list

import com.github.nitrico.lastadapter.StableId
import org.numisoft.vocabuilder.WordCallback
import org.numisoft.vocabuilder.listToString
import org.numisoft.vocabuilder.repository.Word

class WordItemViewModel(
    val word: Word,
    private val showDetailCallback: WordCallback,
    private val deleteCallback: WordCallback
) : StableId {

    override val stableId = word.hashCode().toLong()

    val translation = word.definitions.map { it.translation }.listToString()

    fun onItemClick() = showDetailCallback.invoke(word)

    fun onDeleteClick() = deleteCallback.invoke(word)
}