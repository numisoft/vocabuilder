package org.numisoft.vocabuilder.ui.search

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import org.numisoft.vocabuilder.Resource
import org.numisoft.vocabuilder.repository.Repository
import org.numisoft.vocabuilder.repository.Word
import org.numisoft.vocabuilder.toggle
import org.numisoft.vocabuilder.ui.view.WordCardViewModel

class SearchViewModel(
    private val repository: Repository
) : ViewModel() {

    val input = ObservableField<String>()

    val hideKeyboard = ObservableBoolean()
    val showError = ObservableField<String>()
    val showEmptyState = ObservableField<String>()
    val isProgressVisible = ObservableBoolean()

    val word = ObservableField<Word>()
    val wordCardViewModel = WordCardViewModel(word, viewModelScope)

    fun onSubmitClick() = input.get()?.let { text ->
        if (text.length > 1) {
            repository.getWord(text)
                .onStart {
                    hideKeyboard.toggle()
                    isProgressVisible.set(true)
                }
                .onCompletion { isProgressVisible.set(false) }
                .onEach { processResult(it) }
                .launchIn(viewModelScope)
        }
    }

    private fun processResult(resource: Resource<Word>) = when (resource) {
        is Resource.Success -> word.set(resource.data)
        is Resource.EmptyState -> showEmptyState.set(resource.notFound)
        is Resource.Error -> showError.set(resource.exception?.message)
    }
}