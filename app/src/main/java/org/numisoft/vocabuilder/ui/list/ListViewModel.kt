package org.numisoft.vocabuilder.ui.list

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.numisoft.vocabuilder.WordCallback
import org.numisoft.vocabuilder.repository.Repository
import org.numisoft.vocabuilder.repository.Word

class ListViewModel(
    private val repository: Repository
) : ViewModel() {

    val items = ObservableArrayList<WordItemViewModel>()

    val showDetailDialog = ObservableField<Word>()
    val showDeleteDialog = ObservableField<Word>()

    private val showDetailCallback: WordCallback = {
        showDetailDialog.set(it)
        Log.wtf(javaClass.simpleName, it.toString())
    }

    private val deleteCallback: WordCallback = {
        showDeleteDialog.set(it)
    }

    init {
        repository.getLocalWords()
            .onEach { onSuccess(it) }
            .launchIn(viewModelScope)
    }

    private fun onSuccess(words: List<Word>) {
        val newItems = words.map { WordItemViewModel(it, showDetailCallback, deleteCallback) }
        val itemsToRemove = items.filter { oldItem ->
            newItems.firstOrNull { oldItem.stableId == it.stableId } == null
        }
        val itemsToAdd = newItems.filter { newItem ->
            items.firstOrNull { newItem.stableId == it.stableId } == null
        }
        itemsToRemove.forEach { items.remove(it) }
        items.addAll(itemsToAdd)
        items.sortBy { it.word.text }
    }

    fun deleteWord(word: Word) = CoroutineScope(Dispatchers.IO).launch {
        repository.deleteLocalWord(word)
    }
}