package org.numisoft.vocabuilder

import android.app.Application
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class VocabuilderApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@VocabuilderApp)
            modules(listOf(networkModule, databaseModule, uiModule))
        }

        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)
    }
}